angular.module('starter.controllers', [])

    .controller('AppCtrl', function ($scope, $localStorage, $state) {

        $scope.logout = function(){

            $localStorage.userId = "";
            $state.go('app.login');

        }

    })

    .controller('LoginCtrl', function ($ionicHistory, $ionicPopup, $scope, $localStorage, $http, $httpParamSerializerJQLike, x2js, $state) {

        $scope.$on("$ionicView.enter", function(){

            if($localStorage.userId == "" | !$localStorage.userId || typeof ($localStorage.userId) == 'undefined'){

                $scope.login = {

                    "user" : "",
                    "password" : ""

                };

                $scope.makeLogin = function(){

                    if ($scope.login.user == "" || $scope.login.password == ""){

                        $ionicPopup.alert({
                            title: "נא למלא את כל השדות",
                            buttons: [{
                                text: 'OK',
                                type: 'button-positive'
                            }]
                        });

                    } else {

                        $http.post("http://62.219.199.131/ElAlMobileWS/Service1.asmx/CheckWorkerLogin",
                            $httpParamSerializerJQLike({"userID": $scope.login.user, "password" : $scope.login.password}),

                            {

                                headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
                                paramSerializer: '$httpParamSerializer'

                            })

                            .then(function(data){

                                var response = x2js.xml_str2json(data.data);
                                response = JSON.parse(response.string.toString());
                                console.log(response);

                                if (response[0].UseID != "0"){

                                    $localStorage.userId = response[0].UseID;

                                    if (response[0].IsManager == "0"){

                                        $localStorage.isManager = 0;
                                        $state.go('app.worker');

                                    } else {

                                        $localStorage.isManager = 1;
                                        $state.go('app.manager');

                                    }

                                } else {

                                    $ionicPopup.alert({
                                        title: "נתונים לא נכונים!",
                                        buttons: [{
                                            text: 'OK',
                                            type: 'button-positive'
                                        }]
                                    });

                                }

                            }, function(err){

                                $ionicPopup.alert({
                                    title: "אין התחברות!",
                                    buttons: [{
                                        text: 'OK',
                                        type: 'button-positive'
                                    }]
                                });

                            })


                    }

                }

            } else {

                $ionicHistory.nextViewOptions({
                    disableAnimate: true,
                    disableBack: true
                });

                if ($localStorage.isManager == 0){

                    $state.go("app.worker");

                } else {

                    $state.go("app.manager");

                }

            }

        });

    })

    .controller('WorkerCtrl', function ($ionicLoading, Upload, $filter, $scope, $http, $httpParamSerializerJQLike, $localStorage, x2js, $ionicPopup) {

        $scope.$on("$ionicView.enter", function(){

            $scope.getCurMonthAttendance($scope.firstDayOfMonth, $scope.lastDayOfMonth);

        });

        $scope.angular = angular;

        // functions for proper work of the calendar
        // first getting today,  then - first day of month and last day of month, and don't forget about total month time

        $scope.curDate = new Date();
        $scope.firstDayOfMonth = $filter('date')($scope.curDate, "01/MM/yyyy");
        $scope.lastDayOfMonth = new Date($filter('date')($scope.curDate, "yyyy"), $filter('date')($scope.curDate, "MM"), 0);
        $scope.lastDayOfMonth = $filter('date')($scope.lastDayOfMonth, "dd/MM/yyyy");
        $scope.totalTime = "00.00";

        // $scope.daysInMonth = Array.apply(null, {length: new Date($scope.curDate.getFullYear(), $scope.curDate.getMonth() + 1, 0).getDate()}).map(Number.call, Number);

        $scope.addMonth = function(){

            $scope.curDate.setMonth($scope.curDate.getMonth() + 1);
            $scope.firstDayOfMonth = $filter('date')($scope.curDate, "01/MM/yyyy");
            $scope.lastDayOfMonth = new Date($filter('date')($scope.curDate, "yyyy"), $filter('date')($scope.curDate, "MM"), 0);
            $scope.lastDayOfMonth = $filter('date')($scope.lastDayOfMonth, "dd/MM/yyyy");
            $scope.totalTime = "00.00";
            $scope.getCurMonthAttendance($scope.firstDayOfMonth, $scope.lastDayOfMonth);

        };

        $scope.subtractMonth = function(){

            $scope.curDate.setMonth($scope.curDate.getMonth() - 1);
            $scope.firstDayOfMonth = $filter('date')($scope.curDate, "01/MM/yyyy");
            $scope.lastDayOfMonth = new Date($filter('date')($scope.curDate, "yyyy"), $filter('date')($scope.curDate, "MM"), 0);
            $scope.lastDayOfMonth = $filter('date')($scope.lastDayOfMonth, "dd/MM/yyyy");
            $scope.totalTime = "00.00";
            $scope.getCurMonthAttendance($scope.firstDayOfMonth, $scope.lastDayOfMonth);

        };

        $scope.addYear = function(){

            $scope.curDate.setYear($scope.curDate.getFullYear() + 1);
            $scope.firstDayOfMonth = $filter('date')($scope.curDate, "01/MM/yyyy");
            $scope.lastDayOfMonth = new Date($filter('date')($scope.curDate, "yyyy"), $filter('date')($scope.curDate, "MM"), 0);
            $scope.lastDayOfMonth = $filter('date')($scope.lastDayOfMonth, "dd/MM/yyyy");
            $scope.totalTime = "00.00";
            $scope.getCurMonthAttendance($scope.firstDayOfMonth, $scope.lastDayOfMonth);

        };

        $scope.subtractYear = function(){

            $scope.curDate.setYear($scope.curDate.getFullYear() - 1);
            $scope.firstDayOfMonth = $filter('date')($scope.curDate, "01/MM/yyyy");
            $scope.lastDayOfMonth = new Date($filter('date')($scope.curDate, "yyyy"), $filter('date')($scope.curDate, "MM"), 0);
            $scope.lastDayOfMonth = $filter('date')($scope.lastDayOfMonth, "dd/MM/yyyy");
            $scope.totalTime = "00.00";
            $scope.getCurMonthAttendance($scope.firstDayOfMonth, $scope.lastDayOfMonth);

        };

        // getting work hours for the current month (according to which month is current in the calendar)

        $scope.curMonthAttendance = [];

        $scope.getCurMonthAttendance = function(x, y){

            $scope.curMonthAttendance = [];
            $scope.totalTime = "00.00";

            $http.post("http://62.219.199.131/ElAlMobileWS/Service1.asmx/GetWorkingAttendance",
                $httpParamSerializerJQLike({"userid": $localStorage.userId, "attendanceFromDate" : x, "attendanceToDate" : y}),

                {

                    headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
                    paramSerializer: '$httpParamSerializer'

                })

                .then(function(data){

                    var response = x2js.xml_str2json(data.data);
                    $scope.curMonthAttendance = JSON.parse(response.string.toString());

                    for (var i = 0; i < $scope.curMonthAttendance.length; i++){

                        $scope.curMonthAttendance[i].Cur_Date = $scope.curMonthAttendance[i].Cur_Date.substring(0, $scope.curMonthAttendance[i].Cur_Date.indexOf(' '));
                        $scope.curMonthAttendance[i].Cur_Date = $scope.curMonthAttendance[i].Cur_Date.split("/");
                        $scope.curMonthAttendance[i].Cur_Date = new Date($scope.curMonthAttendance[i].Cur_Date[2], $scope.curMonthAttendance[i].Cur_Date[1] - 1, $scope.curMonthAttendance[i].Cur_Date[0]);

                        $scope.curMonthAttendance[i].id = i;

                        if ($scope.curMonthAttendance[i].IN122 !== "" && $scope.curMonthAttendance[i].OUT122 !== ""){

                            $scope.curMonthAttendance[i].diff = $scope.msToTimeTotal($scope.curMonthAttendance[i].IN122, $scope.curMonthAttendance[i].OUT122);

                        }

                    }

                    console.log($scope.curMonthAttendance);

                }, function(err){

                    $ionicPopup.alert({
                        title: "אין התחברות!",
                        buttons: [{
                            text: 'OK',
                            type: 'button-positive'
                        }]
                    });

                })

        };

        // milliseconds to hours/minutes + calculating total time

        $scope.msToTimeTotal = function(x, y) {

            var timein = x.split(":");
            var timeout = y.split(":");
            var datein = new Date(1970, 0, 1,  timein[0], timein[1]);
            var dateout = new Date(1970, 0, 1, timeout[0], timeout[1]);

            if (dateout < datein) {
                dateout.setDate(dateout.getDate() + 1);
            }

            var s = dateout - datein;

            var ms = s % 1000;
            s = (s - ms) / 1000;
            var secs = s % 60;
            s = (s - secs) / 60;
            var mins = s % 60;
            var hrs = (s - mins) / 60;

            hrs = hrs < 10 ? "0" + hrs : hrs;
            mins = mins < 10 ? "0" + mins : mins;

            var total = $scope.totalTime.split(".");
            total = Number(total[0]*60) + Number(total[1]);
            total = Number(total) + Number(hrs*60) + Number(mins);
            var totalhrs = Math.floor(total / 60) < 10 ? "0" + Math.floor(total / 60) : Math.floor(total / 60);
            var totalmins = total % 60 < 10 ? "0" + total % 60 : total % 60;
            $scope.totalTime = totalhrs + "." + totalmins;

            return hrs + '.' + mins;

        };

        $scope.msToTime = function(x, y) {

            var timein = x.split(":");
            var timeout = y.split(":");
            var datein = new Date(1970, 0, 1,  timein[0], timein[1]);
            var dateout = new Date(1970, 0, 1, timeout[0], timeout[1]);

            if (dateout < datein) {
                dateout.setDate(dateout.getDate() + 1);
            }

            var s = dateout - datein;

            var ms = s % 1000;
            s = (s - ms) / 1000;
            var secs = s % 60;
            s = (s - secs) / 60;
            var mins = s % 60;
            var hrs = (s - mins) / 60;

            hrs = hrs < 10 ? "0" + hrs : hrs;
            mins = mins < 10 ? "0" + mins : mins;

            return hrs + ':' + mins;

        };

        // check in

        $scope.fromTime = {};
        $scope.toTime = {};
        $scope.type = {};
        $scope.isCheckedIn = {};
        console.log($scope.isCheckedIn);

        $scope.makeCheckIn = function(x, y){

            console.log(x, y);

            $localStorage.checkInTime = $scope.fromTime[x];
            $localStorage.checkInDate = String(y);

            $ionicPopup.alert({
                title: "Successfully remembered!",
                buttons: [{
                    text: 'OK',
                    type: 'button-positive'
                }]
            });

            $scope.isCheckedIn = {0 : x};

        };

        // check out

        $scope.makeCheckOut = function(x, y){

            console.log(x, y);
            console.log($scope.toTime[x]);

            if ($localStorage.checkInDate != y){

                $ionicPopup.alert({
                    title: "Please make check out for the day marked as 'Done'!",
                    buttons: [{
                        text: 'OK',
                        type: 'button-positive'
                    }]
                });

            } else {

                var total = $scope.msToTime($localStorage.checkInTime, $scope.toTime[x]);
                console.log(total);

                $http.post("http://62.219.199.131/ElAlMobileWS/Service1.asmx/SaveUserAttendanceWS",
                    $httpParamSerializerJQLike({

                        "OvedId": $localStorage.userId,
                        "fordate" : $filter('date')(y, "dd/MM/yyyy"),
                        "inval" : $localStorage.checkInTime,
                        "outval" : $scope.toTime[x],
                        "sach" : total,
                        "smlo122" : "300"

                    }),
                    {

                        headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
                        paramSerializer: '$httpParamSerializer'

                    })

                    .then(function(data){

                        var response = x2js.xml_str2json(data.data);
                        console.log(response.int.toString());

                    }, function(err){

                        $ionicPopup.alert({
                            title: "אין התחברות!",
                            buttons: [{
                                text: 'OK',
                                type: 'button-positive'
                            }]
                        });

                    })

            }

        };

        // get info about hours according to dates

        $scope.dates = {

            "from" : "",
            "to" : ""

        };

        $scope.datesMode = false;

        $scope.getAttendance = function(){

            if (new Date($scope.dates.from) > new Date($scope.dates.to)){

                $ionicPopup.alert({
                    title: "From-date must be lesser than To-date!",
                    buttons: [{
                        text: 'OK',
                        type: 'button-positive'
                    }]
                });

            } else if ($scope.dates.from !== "" && $scope.dates.to !== ""){

                var diffInDays = Math.abs((new Date($scope.dates.from) - new Date($scope.dates.to)) / 1000 / 60/ 60 / 24);

                if (diffInDays > 90) {

                    $ionicPopup.alert({
                        title: "90 days only!",
                        buttons: [{
                            text: 'OK',
                            type: 'button-positive'
                        }]
                    });

                } else {

                    $scope.getCurMonthAttendance($scope.dates.from, $scope.dates.to);
                    $scope.datesMode = true;

                    $scope.dates = {

                        "from" : "",
                        "to" : ""

                    };


                }

            }

        };

        // upload something

        $scope.image = "";

        $scope.upload = function (file) {

            $scope.image = file;
            console.log($scope.image);

            Upload.base64DataUrl(file).then(

                function(data){

                    // console.log(data);

                    $ionicLoading.show({

                        template: 'טעון...'

                    }).then(function(){

                        $http.post("http://62.219.199.131/ElAlMobileWS/Service1.asmx/WorkerDocumentsSave",
                            $httpParamSerializerJQLike({

                                "filedata" : data,
                                "userID" : Number($localStorage.userId),
                                "attachdate" : String($filter('date')($scope.curDate, "dd/MM/yyyy")),
                                "ContentType" : $scope.image.type,
                                "docName" : ""

                            }),
                            {

                                headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
                                paramSerializer: '$httpParamSerializer'

                            })

                            .then(function(data){

                                var response = x2js.xml_str2json(data.data);
                                alert(response.int.toString());

                                $ionicLoading.hide();

                            }, function(err){

                                $ionicPopup.alert({
                                    title: "אין התחברות!",
                                    buttons: [{
                                        text: 'OK',
                                        type: 'button-positive'
                                    }]
                                });

                                $ionicLoading.hide();

                            });

                    })

                })
        };

    })

    .controller('ManagerCtrl', function ($scope, $http, $httpParamSerializerJQLike, $localStorage, x2js, $ionicPopup) {

        $scope.foundWorkers = [];

        $scope.search = {

            "user" : "",
            "IDnumber" : ""

        };

        $scope.makeSearch = function(){

            if($scope.search.user == "" && $scope.search.IDnumber == ""){

                $ionicPopup.alert({
                    title: "Please fill up at least one field!",
                    buttons: [{
                        text: 'OK',
                        type: 'button-positive'
                    }]
                });

            } else {

                $scope.foundWorkers = [];

                $http.post("http://62.219.199.131/ElAlMobileWS/Service1.asmx/GetWorkers",
                    $httpParamSerializerJQLike({

                        "userid": $localStorage.userId,
                        "FirstName" : $scope.search.user,
                        "ID" : $scope.search.IDnumber

                    }),
                    {

                        headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
                        paramSerializer: '$httpParamSerializer'

                    })

                    .then(function(data){

                        var response = x2js.xml_str2json(data.data);
                        $scope.foundWorkers = JSON.parse(response.string.toString());
                        console.log($scope.foundWorkers);

                        $scope.search = {

                            "user" : "",
                            "IDnumber" : ""

                        };

                    }, function(err){

                        $ionicPopup.alert({
                            title: "אין התחברות!",
                            buttons: [{
                                text: 'OK',
                                type: 'button-positive'
                            }]
                        });

                    })

            }

        };

        $scope.getWorker = function(x){

            console.log(x);

        }

    })

    .controller('AddCtrl', function ($scope, $http, $httpParamSerializerJQLike, $localStorage, x2js, $ionicPopup) {

        $scope.$on("$ionicView.enter", function(){

            $scope.getAgreements();
            $scope.getDivisions();
            $scope.getFactories();
            $scope.getMahlakot();
            $scope.getPositions();

        });

        $scope.agreements = [];
        $scope.divisions = [];
        $scope.factories = [];
        $scope.departments = [];
        $scope.positions = [];

        $scope.getAgreements = function(){

            $scope.agreements = [];

            $http.post("http://62.219.199.131/ElAlMobileWS/Service1.asmx/GetAgreements",
                $httpParamSerializerJQLike({"OvedId": $localStorage.userId}),

                {

                    headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
                    paramSerializer: '$httpParamSerializer'

                })

                .then(function(data){

                    var response = x2js.xml_str2json(data.data);
                    $scope.agreements = JSON.parse(response.string.toString());
                    console.log("Agreements", $scope.agreements);

                }, function(err){

                    $ionicPopup.alert({
                        title: "אין התחברות!",
                        buttons: [{
                            text: 'OK',
                            type: 'button-positive'
                        }]
                    });

                })

        };

        $scope.getDivisions = function(){

            $scope.divisions = [];

            $http.post("http://62.219.199.131/ElAlMobileWS/Service1.asmx/GetDivisionsWS",
                $httpParamSerializerJQLike({"OvedId": $localStorage.userId}),

                {

                    headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
                    paramSerializer: '$httpParamSerializer'

                })

                .then(function(data){

                    var response = x2js.xml_str2json(data.data);
                    $scope.divisions = JSON.parse(response.string.toString());
                    console.log("Divisions", $scope.divisions);

                }, function(err){

                    $ionicPopup.alert({
                        title: "אין התחברות!",
                        buttons: [{
                            text: 'OK',
                            type: 'button-positive'
                        }]
                    });

                })

        };

        $scope.getFactories = function(){

            $scope.factories = [];

            $http.post("http://62.219.199.131/ElAlMobileWS/Service1.asmx/GetFactoriesWS",
                $httpParamSerializerJQLike({"OvedId": $localStorage.userId}),

                {

                    headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
                    paramSerializer: '$httpParamSerializer'

                })

                .then(function(data){

                    var response = x2js.xml_str2json(data.data);
                    $scope.factories = JSON.parse(response.string.toString());
                    console.log("Factories", $scope.factories);

                }, function(err){

                    $ionicPopup.alert({
                        title: "אין התחברות!",
                        buttons: [{
                            text: 'OK',
                            type: 'button-positive'
                        }]
                    });

                })

        };

        $scope.getMahlakot = function() {

            $scope.departments = [];

            $http.post("http://62.219.199.131/ElAlMobileWS/Service1.asmx/GetMahlakotWS",
                $httpParamSerializerJQLike({"OvedId": $localStorage.userId}),

                {

                    headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
                    paramSerializer: '$httpParamSerializer'

                })

                .then(function (data) {

                    var response = x2js.xml_str2json(data.data);
                    $scope.departments = JSON.parse(response.string.toString());
                    console.log("Mahlakot", $scope.departments);

                }, function (err) {

                    $ionicPopup.alert({
                        title: "אין התחברות!",
                        buttons: [{
                            text: 'OK',
                            type: 'button-positive'
                        }]
                    });

                })

        };

        $scope.getPositions = function() {

            $scope.positions = [];

            $http.post("http://62.219.199.131/ElAlMobileWS/Service1.asmx/GetPositions",
                $httpParamSerializerJQLike({"OvedId": $localStorage.userId}),

                {

                    headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
                    paramSerializer: '$httpParamSerializer'

                })

                .then(function (data) {

                    var response = x2js.xml_str2json(data.data);
                    $scope.positions = JSON.parse(response.string.toString());
                    console.log("Positions", $scope.positions);

                }, function (err) {

                    $ionicPopup.alert({
                        title: "אין התחברות!",
                        buttons: [{
                            text: 'OK',
                            type: 'button-positive'
                        }]
                    });

                })

        };

        $scope.newWorker = {

            "name" : "",
            "factory" : "0",
            "division" : "0",
            "department" : "0",
            "position" : "0",
            "IDnumber" : "",
            "agreement" : "1",
            "card" : ""

        };

        $scope.addWorker = function(){

            var firstname = $scope.newWorker.name.substring(0, $scope.newWorker.name.indexOf(' '));
            var lastname = $scope.newWorker.name.substring($scope.newWorker.name.indexOf(' '));

            console.log($scope.newWorker);

            $http.post("http://62.219.199.131/ElAlMobileWS/Service1.asmx/SaveWorkers",
                $httpParamSerializerJQLike({

                    "firstName": firstname,
                    "lastName": lastname,
                    "mifal": Number($scope.newWorker.factory),
                    "agaf": Number($scope.newWorker.division),
                    "mahlaka": Number($scope.newWorker.department),
                    "tafkid": Number($scope.newWorker.position),
                    "id": Number($scope.newWorker.IDnumber),
                    "heskem": Number($scope.newWorker.agreement),
                    "kartis": $scope.newWorker.card

                }),
                {

                    headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
                    paramSerializer: '$httpParamSerializer'

                })

                .then(function(data){

                    var response = x2js.xml_str2json(data.data);
                    response = response.int.toString();
                    console.log(response);

                }, function(err){

                    $ionicPopup.alert({
                        title: "אין התחברות!",
                        buttons: [{
                            text: 'OK',
                            type: 'button-positive'
                        }]
                    });

                })

        }

    })

    .controller('StatisticsCtrl', function ($scope, $http, $httpParamSerializerJQLike, $localStorage, x2js, $ionicPopup) {

        $scope.$on("$ionicView.enter", function(){

            $scope.getStatistics();

        });

        $scope.cell1 = 0;
        $scope.cell2 = 0;
        $scope.cell3 = 0;
        $scope.cell4 = 0;

        $scope.getStatistics = function(){

            $scope.cell1 = 0;
            $scope.cell2 = 0;
            $scope.cell3 = 0;
            $scope.cell4 = 0;

            $http.post("http://62.219.199.131/ElAlMobileWS/Service1.asmx/GetReportsWS",
                $httpParamSerializerJQLike({"OvedId": $localStorage.userId, 'year' : '2016', 'month' : '11'}),

                {

                    headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
                    paramSerializer: '$httpParamSerializer'

                })

                .then(function(data){

                    var response = x2js.xml_str2json(data.data);
                    response = JSON.parse(response.string.toString());
                    console.log(response);
                    $scope.cell1 = response[0].SemelValueMenucha;
                    $scope.cell2 = response[0].SemelValueHofasha;
                    $scope.cell3 = response[0].SemelValueMahaLa;
                    $scope.cell4 = response[0].SemelValueYemeyMenuha;

                }, function(err){

                    $ionicPopup.alert({
                        title: "אין התחברות!",
                        buttons: [{
                            text: 'OK',
                            type: 'button-positive'
                        }]
                    });

                })

        };

    })
;
angular.module('starter.controllers', [])

    .controller('AddCtrl', function (ElAlWebService, AuthService, $scope, $localStorage, $state, $ionicPopup, $http) {

        $scope.$on("$ionicView.enter", function () {

            getAgreements();
            getDivisions();
            getFactories();
            getDepartments();
            getPositions();

        });

        // Data

        $scope.agreements = [];
        $scope.divisions = [];
        $scope.factories = [];
        $scope.departments = [];
        $scope.positions = [];
        $scope.newWorker = {

            "name": "",
            "factory": "0",
            "division": "0",
            "department": "0",
            "position": "0",
            "IDnumber": "",
            "agreement": "1",
            "card": ""

        };

        // Methods

        $scope.addWorker = addWorker;

        // Functions

        function getAgreements() {

            $scope.agreements = [];

            $http.post(ElAlWebService.AGREEMENTS, {"OvedId": AuthService.getUser()}).then(successAgreements, failure);

        }

        function successAgreements(data) {

            $scope.agreements = data;
            console.log("Agreements", $scope.agreements);

        }

        function getDivisions() {

            $scope.divisions = [];

            $http.post(ElAlWebService.DIVISIONS, {"OvedId": AuthService.getUser()}).then(successDivisions, failure);

        }

        function successDivisions(data) {

            $scope.divisions = data;
            console.log("Divisions", $scope.divisions);

        }

        function getFactories() {

            $scope.factories = [];

            $http.post(ElAlWebService.FACTORIES, {"OvedId": AuthService.getUser()}).then(successFactories, failure);

        }

        function successFactories(data) {

            $scope.factories = data;
            console.log("Factories", $scope.factories);

        }

        function getDepartments() {

            $scope.departments = [];

            $http.post(ElAlWebService.DEPARTMENTS, {"OvedId": AuthService.getUser()}).then(successDepartments, failure);

        }

        function successDepartments(data) {

            $scope.departments = data;
            console.log("Departments", $scope.departments);

        }

        function getPositions() {

            $scope.positions = [];

            $http.post(ElAlWebService.POSITIONS, {"OvedId": AuthService.getUser()}).then(successPositions, failure);

        }

        function successPositions(data) {

            $scope.positions = data;
            console.log("Positions", $scope.positions);

        }

        function addWorker() {

            var firstname = $scope.newWorker.name.substring(0, $scope.newWorker.name.indexOf(' '));
            var lastname = $scope.newWorker.name.substring($scope.newWorker.name.indexOf(' '));

            $http.post("http://62.219.199.131/ElAlMobileWS/Service1.asmx/SaveWorkers",
                {

                    "firstName": firstname,
                    "lastName": lastname,
                    "mifal": Number($scope.newWorker.factory),
                    "agaf": Number($scope.newWorker.division),
                    "mahlaka": Number($scope.newWorker.department),
                    "tafkid": Number($scope.newWorker.position),
                    "id": Number($scope.newWorker.IDnumber),
                    "heskem": Number($scope.newWorker.agreement),
                    "kartis": Number($scope.newWorker.card)

                }).then(successAddWorker, failure);
        }

        function successAddWorker(data) {

            if (data == 1) {

                $ionicPopup.alert({
                    title: "You have successfully added an worker!",
                    buttons: [{
                        text: 'OK',
                        type: 'button-positive'
                    }]
                });

                $scope.newWorker = {

                    "name": "",
                    "factory": "0",
                    "division": "0",
                    "department": "0",
                    "position": "0",
                    "IDnumber": "",
                    "agreement": "1",
                    "card": ""

                };

            } else {

                $ionicPopup.alert({
                    title: "The worker was not added!",
                    buttons: [{
                        text: 'OK',
                        type: 'button-positive'
                    }]
                });

            }

        }

        function failure(err) {

            $ionicPopup.alert({
                title: "אין התחברות!",
                buttons: [{
                    text: 'OK',
                    type: 'button-positive'
                }]
            });

        }

    })

    .controller('AppCtrl', function ($scope, $localStorage, $state, AuthService) {

        $scope.$on("$ionicView.enter", function () {

            $scope.isManager = AuthService.isManager();

        });

        $scope.logout = function () {

            AuthService.logout();

            $state.go('app.login');

        }

    })

    .controller('AttendanceCtrl', function ($ionicPopover, ElAlApiService, $ionicLoading, Upload, $ionicPopup, $scope, $http, $localStorage, $state, AuthService, ElAlWebService, moment) {

        $scope.$on("$ionicView.enter", function () {

            $ionicLoading.show({

                template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'

            }).then(function(){

                ElAlApiService.getCurMonthAttendance(AuthService.getUser(), $scope.curDate.clone().startOf('month'), $scope.curDate.clone().endOf('month')).then(success, failure);

                ElAlApiService.getSmalim(AuthService.getUser()).then(successSmalim, failure);

            });

        });

        // Data

        $scope.curDate = moment();
        $scope.curMonthAttendance = [];
        $scope.datesMode = false;
        $scope.totalTime = "00.00";
        $scope.smalim = [];
        $scope.type = {};
        $scope.isCheckedIn = $localStorage.isCheckedIn;
        $scope.image = "";
        $scope.dates = {"from": "", "to": ""};
        $scope.date = {time: moment()};
        $scope.dayid = '';
        $scope.line = '';
        $scope.userType = '';
        $scope.checkin = '';
        $scope.checkout = '';
        $scope.uploadDate = '';

        // Methods

        $scope.addMonth = addMonth;
        $scope.subtractMonth = subtractMonth;
        $scope.addYear = addYear;
        $scope.subtractYear = subtractYear;
        $scope.makeCheckIn = makeCheckIn;
        $scope.makeCheckOut = makeCheckOut;
        $scope.upload = upload;
        $scope.getAttendance = getAttendance;
        $scope.showInPopup = showInPopup;
        $scope.hideInPopup = hideInPopup;
        $scope.showOutPopup = showOutPopup;
        $scope.hideOutPopup = hideOutPopup;
        $scope.openPopover = openPopover;

        // Functions

        $ionicPopover.fromTemplateUrl('templates/popover.html', {scope: $scope})
            .then(function(popover) {$scope.popover = popover;});

        function openPopover ($event, date) {

            $scope.popover.show($event);
            $scope.uploadDate = date;

        }

        function success (data) {

            $scope.curMonthAttendance = data.curMonthAttendance;
            console.log("curMonthAttendance", $scope.curMonthAttendance);

            $scope.totalTime = data.totalTime;

            $ionicLoading.hide();
        }

        function successSmalim(response) {

            $scope.smalim = response.data;
            console.log("Smalim ", $scope.smalim);

            $ionicLoading.hide();

        }

        function addMonth() {

            $scope.curDate.add(1, 'month');

            $ionicLoading.show({

                template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'

            }).then(function(){

                ElAlApiService.getCurMonthAttendance(AuthService.getUser(), $scope.curDate.clone().startOf('month'), $scope.curDate.clone().endOf('month')).then(success, failure);

            })
        }

        function subtractMonth() {

            $scope.curDate.subtract(1, 'month');

            $ionicLoading.show({

                template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'

            }).then(function(){

                ElAlApiService.getCurMonthAttendance(AuthService.getUser(), $scope.curDate.clone().startOf('month'), $scope.curDate.clone().endOf('month')).then(success, failure);

            })
        }

        function addYear() {

            $scope.curDate.add(1, 'year');

            $ionicLoading.show({

                template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'

            }).then(function(){

                ElAlApiService.getCurMonthAttendance(AuthService.getUser(), $scope.curDate.clone().startOf('month'), $scope.curDate.clone().endOf('month')).then(success, failure);

            })
        }

        function subtractYear() {

            $scope.curDate.subtract(1, 'year');

            $ionicLoading.show({

                template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'

            }).then(function(){

                ElAlApiService.getCurMonthAttendance(AuthService.getUser(), $scope.curDate.clone().startOf('month'), $scope.curDate.clone().endOf('month')).then(success, failure);

            })
        }

        function showInPopup(id, date, type, checkout){

            $scope.date = {time: moment()};

            $scope.inPopup = $ionicPopup.show({
                templateUrl: 'templates/in_popup.html',
                scope: $scope,
                cssClass: 'timePopup'
            });

            $scope.checkout = checkout != '' ? checkout : '00:00';
            $scope.dayid = id;
            $scope.line = date;
            $scope.userType = type[id];

        }

        function hideInPopup(){

            $scope.inPopup.close();

            $scope.checkout = '';
            $scope.dayid = '';
            $scope.line = '';
            $scope.userType = '';

        }

        function makeCheckIn(){

            $http.post(ElAlWebService.CHECKOUT,
                {

                    "OvedId": AuthService.getUser(),
                    "Taarih": moment($scope.line).format('DD/MM/YYYY 00:00:00'),
                    "Enter": moment($scope.line).format('DD/MM/YYYY') + ' ' + moment($scope.date.time).format('HH:mm:ss'),
                    "Exit": moment($scope.line).format('DD/MM/YYYY') + ' ' + moment($scope.checkout, 'HH:mm').format('HH:mm:00'),
                    "smlo122": $scope.userType

                }).then(successCheckIn, failure);

        }

        function successCheckIn (data) {

            $localStorage.isCheckedIn = {0: $scope.dayid};
            $scope.isCheckedIn = $localStorage.isCheckedIn;

            $scope.inPopup.close();
            $ionicPopup.alert({title: "Successfully remembered!"});

            ElAlApiService.getCurMonthAttendance(AuthService.getUser(), $scope.curDate.clone().startOf('month'), $scope.curDate.clone().endOf('month')).then(success, failure);


        }

        function showOutPopup(id, date, type, checkin){

            $scope.date = {time: moment()};

            $scope.outPopup = $ionicPopup.show({
                templateUrl: 'templates/out_popup.html',
                scope: $scope,
                cssClass: 'timePopup'
            });

            $scope.checkin = checkin != '' ? checkin : '00:00';
            $scope.dayid = id;
            $scope.line = date;
            $scope.userType = type[id];

        }

        function hideOutPopup(){

            $scope.dayid = '';
            $scope.checkin = '';
            $scope.line = '';
            $scope.userType = '';
            $scope.outPopup.close();

        }

        // make check out on the device

        function makeCheckOut() {

            var exitDay = moment($scope.date.time).isBefore(moment($scope.checkin, 'HH:mm'), 'hours') ? moment($scope.line).add(1, 'days').format('DD/MM/YYYY') : moment($scope.line).format('DD/MM/YYYY');

            $http.post(ElAlWebService.CHECKOUT,
                {

                    "OvedId": AuthService.getUser(),
                    "Taarih": moment($scope.line).format('DD/MM/YYYY 00:00:00'),
                    "Enter": moment($scope.line).format('DD/MM/YYYY') + ' ' + moment($scope.checkin, 'HH:mm').format('HH:mm:00'),
                    "Exit": exitDay + ' ' + moment($scope.date.time).format('HH:mm:ss'),
                    "smlo122": $scope.userType

                }).then(successCheckOut, failure);

        }

        function successCheckOut(data) {

            $scope.outPopup.close();

            if (data > 0) {

                $ionicPopup.alert({
                    title: "Successfully updated!",
                    buttons: [{
                        text: 'OK',
                        type: 'button-positive'
                    }]
                });

                $scope.checkin = '';

                ElAlApiService.getCurMonthAttendance(AuthService.getUser(), $scope.curDate.clone().startOf('month'), $scope.curDate.clone().endOf('month')).then(success, failure);

            } else {

                $ionicPopup.alert({
                    title: "The error occured, please try again!",
                    buttons: [{
                        text: 'OK',
                        type: 'button-positive'
                    }]
                });

            }

        }

        // upload something

        function upload(file, date) {

            date = typeof date === 'undefined' ? $scope.uploadDate : date;

            $ionicLoading.show({

                template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'

            }).then(function(){

                $scope.image = file;
                console.log($scope.image);

                Upload.upload({
                    url: ElAlWebService.UPLOAD,
                    data: {

                        "filedata": $scope.image,
                        "userID": AuthService.getUser(),
                        "attachdate": moment(date).format('DD/MM/YYYY').toString(),
                        "ContentType": $scope.image.type,
                        "docName": $scope.image.name

                    }
                }).then(successUpload, failureUpload);

            })

        }

        function successUpload(data) {

            $ionicLoading.hide();
            $scope.image = '';

            if (data == 1){

                $ionicPopup.alert({title: "!המסמך נשמר בהצלחה"});
                ElAlApiService.getCurMonthAttendance(AuthService.getUser(), $scope.curDate.clone().startOf('month'), $scope.curDate.clone().endOf('month')).then(success, failure);

            } else
                $ionicPopup.alert({title: "אין התחברות!"});


        }

        function failureUpload(err) {

            $ionicPopup.alert({
                title: "אין התחברות!",
                buttons: [{
                    text: 'OK',
                    type: 'button-positive'
                }]
            });
            $ionicLoading.hide();
            $scope.image = '';

        }

        // Failure function for almost everything in this controller

        function failure(errors) {

            console.log(errors);

            $ionicLoading.hide();

            $ionicPopup.alert({
                title: "אין התחברות!",
                buttons: [{
                    text: 'OK',
                    type: 'button-positive'
                }]
            });

        }

        function getAttendance() {

            if (moment($scope.dates.from, 'YYYY-MM-DD') > moment($scope.dates.to, 'YYYY-MM-DD')) {

                $ionicPopup.alert({
                    title: "From-date must be lesser than To-date!",
                    buttons: [{
                        text: 'OK',
                        type: 'button-positive'
                    }]
                });

            } else if ($scope.dates.from !== "" && $scope.dates.to !== "") {

                var diff = moment.duration(moment($scope.dates.to, 'YYYY-MM-DD').diff(moment($scope.dates.from, 'YYYY-MM-DD')));
                var years = diff.years();
                var months = diff.months();
                var days = diff.days();

                var diffInDays = years * 365 + months * 30 + days;

                if (diffInDays > 90) {

                    $ionicPopup.alert({
                        title: "90 days only!",
                        buttons: [{
                            text: 'OK',
                            type: 'button-positive'
                        }]
                    });

                } else {

                    $ionicLoading.show({

                        template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'

                    }).then(function(){

                        ElAlApiService.getCurMonthAttendance(AuthService.getUser(), moment($scope.dates.from, 'YYYY-MM-DD'), moment($scope.dates.to, 'YYYY-MM-DD')).then(success, failure);
                        $scope.datesMode = true;
                        $scope.dates = {"from": "", "to": ""};

                    });

                }

            }

        }

    })

    .controller('LoginCtrl', function ($ionicLoading, AuthService, $ionicHistory, $ionicPopup, $scope, $localStorage, $http, $httpParamSerializerJQLike, x2js, $state) {

        // Data

        $scope.login = {user: '', password: ''};

        // Methods

        $scope.doLogin = doLogin;

        // Functions

        function doLogin() {

            if ($scope.login.user == "" || $scope.login.password == "") {

                $ionicPopup.alert({
                    title: "נא למלא את כל השדות",
                    buttons: [{
                        text: 'OK',
                        type: 'button-positive'
                    }]
                });

                return;

            }

            $ionicLoading.show({

                template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'

            }).then(function(){

                AuthService
                    .authenticate($scope.login.user, $scope.login.password)
                    .then(success, failure);

            });

        }

        function success() {

            if (AuthService.isManager())
                $state.go('app.manager');
            else
                $state.go('app.attendance');

            $scope.login = {user: '', password: ''};

            $ionicLoading.hide();

        }

        function failure(errors) {

            $ionicLoading.hide();

            if (!errors) {

                $ionicPopup.alert({
                    title: "אין התחברות!",
                    buttons: [{
                        text: 'OK',
                        type: 'button-positive'
                    }]
                });

                return;

            }

            if (errors.error == 'invalidCredentials') {

                $ionicPopup.alert({
                    title: "נתונים לא נכונים!",
                    buttons: [{
                        text: 'OK',
                        type: 'button-positive'
                    }]
                });

            }

        }

    })

    .controller('ManagerCtrl', function ($ionicLoading, $state, $stateParams, $rootScope, $ionicPopup, AuthService, ElAlWebService, $http, $scope) {

        // Data

        $scope.foundWorkers = [];
        $scope.search = {"user": "", "IDnumber": ""};

        // Methods

        $scope.makeSearch = makeSearch;
        $scope.getWorker = getWorker;

        // Functions

        // make search

        function makeSearch() {

            if ($scope.search.user == "" && $scope.search.IDnumber == "") {

                $scope.foundWorkers = [];

            } else {

                $scope.foundWorkers = [];

                $ionicLoading.show({

                    template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'

                }).then(function(){

                    $http.post(ElAlWebService.SEARCH,
                        {

                            "userid": "",
                            "FirstName": $scope.search.user,
                            "ID": $scope.search.IDnumber

                        }).then(success, failure)

                });

            }

        }

        function success(data) {

            $ionicLoading.hide();

            $scope.foundWorkers = data;
            console.log('foundWorkers', $scope.foundWorkers);

            if ($scope.foundWorkers.length == 0) {

                $ionicPopup.alert({
                    title: "No results!",
                    buttons: [{
                        text: 'OK',
                        type: 'button-positive'
                    }]
                });

            }

            $scope.search = {"user": "", "IDnumber": ""};

        }

        function failure(errors) {

            $ionicLoading.hide();

            $ionicPopup.alert({
                title: "אין התחברות!",
                buttons: [{
                    text: 'OK',
                    type: 'button-positive'
                }]
            });

        }

        function getWorker(x, y, z) {

            $rootScope.foundWorkerName = y + " " + z;
            $state.go('app.worker', {id: x});

        }

    })

    .controller('StatisticsCtrl', function ($stateParams, ElAlWebService, AuthService, $ionicPopup, x2js, $filter, $http, $scope, $rootScope, $localStorage) {

        $scope.$on("$ionicView.enter", function () {

            getStatistics();

        });

        // Data

        $scope.cell1 = 0;
        $scope.cell2 = 0;
        $scope.cell3 = 0;
        $scope.cell4 = 0;

        // Methods

        $scope.getStatistics = getStatistics;

        // Functions

        function getStatistics() {

            var user = $stateParams.id || AuthService.getUser();

            $scope.cell1 = 0;
            $scope.cell2 = 0;
            $scope.cell3 = 0;
            $scope.cell4 = 0;

            $http.post(ElAlWebService.STATISTICS, {
                "OvedId": user,
                'year': moment().year(),
                'month': Number(moment().month().toString()) + 1
            }).then(success, failure)
        }

        function success(data) {

            $scope.cell1 = data[0].SemelValueMenucha;
            $scope.cell2 = data[0].SemelValueHofasha;
            $scope.cell3 = data[0].SemelValueMahaLa;
            $scope.cell4 = data[0].SemelValueYemeyMenuha;

        }

        function failure(err) {

            $ionicPopup.alert({
                title: "אין התחברות!",
                buttons: [{
                    text: 'OK',
                    type: 'button-positive'
                }]
            });

        }

    })

    .controller('WorkerCtrl', function ($ionicPopover, Upload, $ionicLoading, $http, ElAlWebService, ElAlApiService, $stateParams, $ionicPopup, $scope, $localStorage) {

        $scope.$on("$ionicView.enter", function () {

            $ionicLoading.show({

                template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'

            }).then(function(){

                ElAlApiService.getCurMonthAttendance($scope.id, $scope.curDate.clone().startOf('month'), $scope.curDate.clone().endOf('month')).then(success, failure);

                ElAlApiService.getSmalim($stateParams.id).then(successSmalim, failure);

            });

        });

        $scope.$on("$ionicView.leave", function () {

            $scope.dayid = '';
            $scope.userTypeWorker = '';
            $scope.lineWorker = '';
            $scope.checkinWorker = '';
            $scope.checkoutWorker = '';

        });

        // Data

        $scope.id = $stateParams.id;

        $scope.curDate = moment();
        $scope.curMonthAttendance = [];
        $scope.datesMode = false;
        $scope.totalTime = "00.00";
        $scope.dates = {"from": "", "to": ""};
        $scope.smalim = [];
        $scope.type = {};
        $scope.isCheckedInWorker = $localStorage.isCheckedInWorker;
        $scope.image = "";
        $scope.date = {time: moment()};
        $scope.dayid = '';
        $scope.userTypeWorker = '';
        $scope.lineWorker = '';
        $scope.checkinWorker = '';
        $scope.checkoutWorker = '';
        $scope.uploadDateWorker = '';

        $scope.angular = angular;

        // Methods

        $scope.addMonth = addMonth;
        $scope.subtractMonth = subtractMonth;
        $scope.addYear = addYear;
        $scope.subtractYear = subtractYear;
        $scope.getAttendance = getAttendance;
        $scope.makeCheckIn = makeCheckIn;
        $scope.makeCheckOut = makeCheckOut;
        $scope.upload = upload;
        $scope.showInPopup = showInPopup;
        $scope.hideInPopup = hideInPopup;
        $scope.showOutPopup = showOutPopup;
        $scope.hideOutPopup = hideOutPopup;
        $scope.openPopover = openPopover;

        // Functions

        $ionicPopover.fromTemplateUrl('templates/popover.html', {scope: $scope})
            .then(function(popover) {$scope.popover = popover;});

        function openPopover ($event, date) {

            $scope.popover.show($event);
            $scope.uploadDateWorker = date;

        }


        function success (data) {

            $ionicLoading.hide();

            $scope.curMonthAttendance = data.curMonthAttendance;
            console.log("curMonthAttendance", $scope.curMonthAttendance);

            $scope.totalTime = data.totalTime;

        }

        function successSmalim(response) {

            $ionicLoading.hide();

            $scope.smalim = response.data;
            console.log("Smalim ", $scope.smalim);

        }

        function addMonth() {

            $scope.curDate.add(1, 'month');

            $ionicLoading.show({

                template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'

            }).then(function(){

                ElAlApiService.getCurMonthAttendance($scope.id, $scope.curDate.clone().startOf('month'), $scope.curDate.clone().endOf('month')).then(success, failure);

            });

        }

        function subtractMonth() {

            $scope.curDate.subtract(1, 'month');

            $ionicLoading.show({

                template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'

            }).then(function(){

                ElAlApiService.getCurMonthAttendance($scope.id, $scope.curDate.clone().startOf('month'), $scope.curDate.clone().endOf('month')).then(success, failure);

            });
        }

        function addYear() {

            $scope.curDate.add(1, 'year');

            $ionicLoading.show({

                template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'

            }).then(function(){

                ElAlApiService.getCurMonthAttendance($scope.id, $scope.curDate.clone().startOf('month'), $scope.curDate.clone().endOf('month')).then(success, failure);

            });
        }

        function subtractYear() {

            $scope.curDate.subtract(1, 'year');

            $ionicLoading.show({

                template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'

            }).then(function(){

                ElAlApiService.getCurMonthAttendance($scope.id, $scope.curDate.clone().startOf('month'), $scope.curDate.clone().endOf('month')).then(success, failure);

            });
        }


        function showInPopup(id, date, type, checkout){

            $scope.date = {time: moment()};

            $scope.inPopup = $ionicPopup.show({
                templateUrl: 'templates/in_popup.html',
                scope: $scope,
                cssClass: 'timePopup'
            });

            $scope.checkoutWorker = checkout != '' ? checkout : '00:00';
            $scope.dayid = id;
            $scope.lineWorker = date;
            $scope.userTypeWorker = type[id];

        }

        function hideInPopup(){

            $scope.inPopup.close();

            $scope.checkoutWorker = '';
            $scope.dayid = '';
            $scope.lineWorker = '';
            $scope.lineWorker = '';

        }

        function makeCheckIn(){

            $http.post(ElAlWebService.CHECKOUT,
                {

                    "OvedId": $scope.id,
                    "Taarih": moment($scope.lineWorker).format('DD/MM/YYYY 00:00:00'),
                    "Enter": moment($scope.lineWorker).format('DD/MM/YYYY') + ' ' + moment($scope.date.time).format('HH:mm:ss'),
                    "Exit": moment($scope.lineWorker).format('DD/MM/YYYY') + ' ' + moment($scope.checkoutWorker, 'HH:mm').format('HH:mm:00'),
                    "smlo122": $scope.userTypeWorker

                }).then(successCheckIn, failure);

        }

        function successCheckIn (data) {

            $localStorage.isCheckedInWorker = {0: $scope.dayid};
            $scope.isCheckedInWorker = $localStorage.isCheckedInWorker;

            $scope.inPopup.close();
            $ionicPopup.alert({title: "Successfully remembered!"});

            ElAlApiService.getCurMonthAttendance($scope.id, $scope.curDate.clone().startOf('month'), $scope.curDate.clone().endOf('month')).then(success, failure);


        }

        function showOutPopup(id, date, type, checkin){

            $scope.date = {time: moment()};
            $scope.checkinWorker = checkin;

            $scope.outPopup = $ionicPopup.show({
                templateUrl: 'templates/out_popup.html',
                scope: $scope,
                cssClass: 'timePopup'
            });

            $scope.checkinWorker = checkin != '' ? checkin : '00:00';
            $scope.dayid = id;
            $scope.lineWorker = date;
            $scope.userTypeWorker = type[id];


        }

        function hideOutPopup(){

            $scope.checkinWorker = '';
            $scope.dayid = '';
            $scope.lineWorker = '';
            $scope.userTypeWorker = '';
            $scope.outPopup.close();

        }

        // make check out on the device

        function makeCheckOut() {

            var exitDay = moment($scope.date.time).isBefore(moment($scope.checkinWorker, 'HH:mm'), 'hours') ? moment($scope.lineWorker).add(1, 'days').format('DD/MM/YYYY') : moment($scope.lineWorker).format('DD/MM/YYYY');

            $http.post(ElAlWebService.CHECKOUT,
                {

                    "OvedId": $scope.id,
                    "Taarih": moment($scope.lineWorker).format('DD/MM/YYYY 00:00:00'),
                    "Enter": moment($scope.lineWorker).format('DD/MM/YYYY') + ' ' + moment($scope.checkinWorker, 'HH:mm').format('HH:mm:00'),
                    "Exit": exitDay + ' ' + moment($scope.date.time).format('HH:mm:ss'),
                    "smlo122": $scope.userTypeWorker

                }).then(successCheckOut, failure);

        }

        function successCheckOut(data) {

            $scope.outPopup.close();

            if (data > 0) {

                $ionicPopup.alert({
                    title: "Successfully updated!",
                    buttons: [{
                        text: 'OK',
                        type: 'button-positive'
                    }]
                });

                $scope.checkinWorker = '';

                ElAlApiService.getCurMonthAttendance($scope.id, $scope.curDate.clone().startOf('month'), $scope.curDate.clone().endOf('month')).then(success, failure);

            } else {

                $ionicPopup.alert({
                    title: "The error occured, please try again!",
                    buttons: [{
                        text: 'OK',
                        type: 'button-positive'
                    }]
                });

            }

        }

        // upload something

        function upload(file, date) {

            date = typeof date === 'undefined' ? $scope.uploadDateWorker : date;

            $ionicLoading.show({

                template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'

            }).then(function(){

                $scope.image = file;
                console.log($scope.image);

                Upload.upload({
                    url: ElAlWebService.UPLOAD,
                    data: {

                        "filedata": $scope.image,
                        "userID": $scope.id,
                        "attachdate": moment(date).format('DD/MM/YYYY').toString(),
                        "ContentType": $scope.image.type,
                        "docName": $scope.image.name

                    }
                }).then(successUpload, failureUpload);

            })

        }

        function successUpload(data) {

            $ionicLoading.hide();
            $scope.image = '';

            if (data == 1){

                $ionicPopup.alert({title: "!המסמך נשמר בהצלחה"});
                ElAlApiService.getCurMonthAttendance($scope.id, $scope.curDate.clone().startOf('month'), $scope.curDate.clone().endOf('month')).then(success, failure);

            } else
                $ionicPopup.alert({title: "אין התחברות!"});


        }

        function failureUpload(err) {

            $ionicPopup.alert({
                title: "אין התחברות!",
                buttons: [{
                    text: 'OK',
                    type: 'button-positive'
                }]
            });
            $ionicLoading.hide();
            $scope.image = '';

        }

        // Failure function for almost everything in this controller

        function failure(errors) {

            $ionicLoading.hide();

            console.log(errors);

            $ionicPopup.alert({
                title: "אין התחברות!",
                buttons: [{
                    text: 'OK',
                    type: 'button-positive'
                }]
            });

        }

        function getAttendance() {

            if (moment($scope.dates.from, 'YYYY-MM-DD') > moment($scope.dates.to, 'YYYY-MM-DD')) {

                $ionicPopup.alert({
                    title: "From-date must be lesser than To-date!",
                    buttons: [{
                        text: 'OK',
                        type: 'button-positive'
                    }]
                });

            } else if ($scope.dates.from !== "" && $scope.dates.to !== "") {

                var diff = moment.duration(moment($scope.dates.to, 'YYYY-MM-DD').diff(moment($scope.dates.from, 'YYYY-MM-DD')));
                var years = diff.years();
                var months = diff.months();
                var days = diff.days();

                var diffInDays = years * 365 + months * 30 + days;

                if (diffInDays > 90) {

                    $ionicPopup.alert({
                        title: "90 days only!",
                        buttons: [{
                            text: 'OK',
                            type: 'button-positive'
                        }]
                    });

                } else {

                    $ionicLoading.show({

                        template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'

                    }).then(function(){

                        ElAlApiService.getCurMonthAttendance($scope.id, moment($scope.dates.from, 'YYYY-MM-DD'), moment($scope.dates.to, 'YYYY-MM-DD')).then(success, failure);

                        $scope.datesMode = true;
                        $scope.dates = {"from": "", "to": ""};

                    });

                }

            }

        }

    })

;